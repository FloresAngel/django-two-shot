# Generated by Django 5.0.3 on 2024-03-14 01:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="account",
            name="name",
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name="receipt",
            name="vendor",
            field=models.CharField(max_length=200),
        ),
    ]
