# feat 11
from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account

class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = (
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        )

# Feat 13
class ExpenseCategoryForm(ModelForm):
        class Meta:
            model = ExpenseCategory
            fields = ["name"]

# Feat 14
class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = (
            "name",
            "number",
        )
