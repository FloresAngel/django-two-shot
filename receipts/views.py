from django.shortcuts import render, get_object_or_404, redirect
from receipts.models import ExpenseCategory, Receipt, Account
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.
@login_required
def view_receipts(request):
    view = Receipt.objects.filter( purchaser= request.user)
    context = {
        "receipt_lst": view,
    }
    return render(request, "receipts/list.html", context)

# Feat 11
@login_required
def create_receipt(request):
    if request.method == "POST":
        create = ReceiptForm(request.POST)
        if create.is_valid():
            receipt = create.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        create = ReceiptForm()
    context = {
        "create": create,
    }
    return render(request, "receipts/create.html", context)

#Feat 12
@login_required
def list_accounts(request):
    account = Account.objects.filter(owner=request.user)
    context = {
        "accounts": account,
    }
    return render(request, "receipts/listaccounts.html", context)


@login_required
def list_expenses(request):
    expense = ExpenseCategory.objects.filter(owner=request.user)
    context = {"expenses": expense}
    return render(request, "receipts/listexpenses.html", context)

# Feat 13
@login_required
def create_category(request):
    if request.method == "POST":
        expense_form = ExpenseCategoryForm(request.POST)
        if expense_form.is_valid:
            expenseform = expense_form.save(False)
            expenseform.owner = request.user
            expenseform.save()
            return redirect("category_list")
    else:
        expense_form = ExpenseCategoryForm()
        context = {"exform": expense_form}
    return render(request, "receipts/createexpense.html", context)

# Fet 14
@login_required
def create_account(request):
    if request.method == "POST":
        create_account = AccountForm(request.POST)
        if create_account.is_valid:
            accountform = create_account.save(False)
            accountform.owner = request.user
            accountform.save()
            return redirect("account_list")
    else:
        create_account = AccountForm()
        context = {"account": create_account}
    return render(request, "receipts/createaccount.html", context)

# create_account
