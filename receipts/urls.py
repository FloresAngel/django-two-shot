# Feat 11
from django.urls import path
from receipts.views import (
    view_receipts,
    create_receipt,
    list_accounts,
    list_expenses,
    # Fear 13
    create_category,
    # Fear 14
    create_account,
)


urlpatterns = [
    path("", view_receipts, name="home"),
    path("create/", create_receipt, name="create_receipt"),
# Feat 12
    path("accounts/", list_accounts, name="account_list"),
    path("categories/", list_expenses, name="category_list"),
# Feat 13
    path("categories/create/", create_category, name="create_category"),
# Feat 14
    path("accounts/create/", create_account, name="create_account"),
]
