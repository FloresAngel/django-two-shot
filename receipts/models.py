from django.db import models
from django.conf import settings

# Create your models here.
class ExpenseCategory(models.Model):
    name = models.CharField(max_length = 50)
    owner= models.ForeignKey(
        settings.AUTH_USER_MODEL,
         related_name='categories',
         on_delete=models.CASCADE,
        )
    def __str__(self):
        return self.name

class Account(models.Model):
    name = models.CharField(max_length = 100)
    number = models.CharField(max_length = 20)
    owner = models.ForeignKey(
         settings.AUTH_USER_MODEL,
         related_name='accounts',
         on_delete=models.CASCADE
        )
    def __str__(self):
        return self.name

# Feat 11
class Receipt(models.Model):
    vendor = models.CharField("vendor", max_length=200)
    total = models.DecimalField("total", max_digits=10, decimal_places=3)
    tax = models.DecimalField("tax", max_digits=10, decimal_places=3)
    date = models.DateTimeField("date", null=True, blank=True)
    #assigning values to a variable
    purchaser = models.ForeignKey(settings.AUTH_USER_MODEL, related_name ='receipts', on_delete = models.CASCADE)
    category = models.ForeignKey(ExpenseCategory, related_name = 'receipts', on_delete=models.CASCADE)
    account = models.ForeignKey(Account, related_name = 'receipts', on_delete = models.CASCADE,null = True)
